import time
from equip.SCPI import Rs232
import csv

serial_test = Rs232('ASRL3::INSTR')
serial_test.idn()
# serial_test.name()

with open('serial_data.csv', 'w', newline='') as csvfile:
  csvwriter = csv.writer(csvfile)
  
  for i in range(0,11):
    serial_test.brightness(i)  
    time.sleep(1)
    value = serial_test.brightness_query()
    print (value)

    # Write the formatted data to the CSV file
    # csvwriter.writerow([value])
  serial_test.brightness(0)
  # serial_test.inst.write("SYSTem:LED:BRIGhtness 0")
  time.sleep(1)
  # serial_test.idn()