
import time
import serial
          
ser = serial.Serial(
              
    port='COM4',
    baudrate = 9600,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=1
)
command = str.encode("*IDN?")
ser.write(command)
#print("sent on",now.strftime("%H:%M:%S:%f"))
time.sleep(1)

cc = str(ser.readline())
formatted_data = cc[2:][:-5]

print (formatted_data)