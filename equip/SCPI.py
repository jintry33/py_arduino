import pyvisa
import time

class Rs232():
    def __init__(self, port):
        self.rm = pyvisa.ResourceManager()
        self.inst = self.rm.open_resource(port)
        # self.inst = self.rm.open_resource('ASRL4::INSTR')
        self.inst.read_termination = '\n'
        time.sleep(1)
        self.inst.write_termination = '\n'
        time.sleep(1)

    def idn(self):
        print(self.inst.query("*IDN?"))

    def name(self):
        print(self.inst.query("a"))

    def bright_increse(self):
        print(self.inst.write("SYSTem:LED:BRIGhtness:INCrease"))
    
    def bright_decrese(self):
        print(self.inst.write("SYSTem:LED:BRIGhtness:DECrease"))

    def brightness(self, level):
        self.inst.write("SYSTem:LED:BRIGhtness "+str(level))
    
    def brightness_query(self):
        value = self.inst.query("SYSTem:LED:BRIGhtness?")
        return value
