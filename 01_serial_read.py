import serial
import time
import csv
import matplotlib.pyplot as plt

ser = serial.Serial('COM3', 9600)

x_data = []  # List to store x-axis data (time)
y_data = []  # List to store y-axis data (formatted_data)

# Open a CSV file for writing
with open('serial_data.csv', 'w', newline='') as csvfile:
    csvwriter = csv.writer(csvfile)

    # Create a figure and axis for plotting
    fig, ax = plt.subplots()

    while True:
        cc = str(ser.readline())
        formatted_data = cc[2:][:-5]
        
        # Print the formatted data
        print(formatted_data)

        # Write the formatted data to the CSV file
        csvwriter.writerow([formatted_data])
        
        # Convert the formatted data to a number (if applicable)
        try:
            data_value = float(formatted_data)
            timestamp = time.time()  # Current timestamp

            # Append data to lists
            x_data.append(timestamp)
            y_data.append(data_value)
            
            # Plotting
            ax.plot(x_data, y_data)
            ax.set_xlabel('Time')
            ax.set_ylabel('Value')
            ax.set_title('Serial Data Plot')
            # ax.autofmt_xdate()
            plt.pause(0.1)
            
            # Save the plot as a JPG file
            fig.savefig('serial_data_plot.jpg')

            ax.clear()  # Clear the axis for the next data point
            
        except ValueError:
            pass  # Ignore non-numeric data

        if cc.startswith("b'End"):
            break


